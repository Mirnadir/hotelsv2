$(document).ready(function() {
    // mobile menu [[
        
    // open 
    $('.mobileButton').click(function() {
        $('.c-mobileMenu').removeClass("close-menu")
        $('.c-mobileMenu').addClass("open-menu")
        $('body').css('overflow', 'hidden')
    })

    // close
    $('.c-mobileMenu__item--close img').click(function() {
        $('.c-mobileMenu').addClass("close-menu")
        setTimeout(function() {
            $('.c-mobileMenu').removeClass("open-menu")
            $('body').css('overflow', 'auto')
        }, 400)
    })
    // mobile menu ]]
})
